﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using GradeBook.Enums;


namespace GradeBook.GradeBooks
{
    public class RankedGradeBook : BaseGradeBook
    {
        public RankedGradeBook(string name, bool isWeighted) : base(name, isWeighted)
        {
            Type = GradeBookType.Ranked;
        }

        public override char GetLetterGrade(double averageGrade)
        {
            if (Students.Count < 5)
                throw new InvalidOperationException("Ranked-grading requires a minimum of 5 students to work.");

            var cutoff = (int)Math.Ceiling(Students.Count * 0.20);
            var grades = Students.OrderByDescending(e => e.AverageGrade).Select(e => e.AverageGrade).ToList();

            if (grades[cutoff - 1] <= averageGrade)
                return 'A';
            else if (grades[(cutoff * 2) - 1] <= averageGrade)
                return 'B';
            else if (grades[(cutoff * 3) - 1] <= averageGrade)
                return 'C';
            else if (grades[(cutoff * 4) - 1] <= averageGrade)
                return 'D';
            else
                return 'F';
        }
        public override void CalculateStatistics()
        {
            if (Students.Count < 5)

                Console.WriteLine("Ranked grading requires at least 5 students.");
            else
                base.CalculateStatistics();

        }
        public override void CalculateStudentStatistics(String Name)
        {
            if (Students.Count < 5)

                Console.WriteLine("Ranked grading requires at least 5 students.");
            else
                base.CalculateStudentStatistics(Name);
        }
    }
}
